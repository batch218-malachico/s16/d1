// let intro = "Hello World"
// console.log(intro)

// [SECTION] Assignment Operator
// basic assignment operator (=)
let assignmentNumber = 8;
console.log("The value of assignmentNumber variable: "+ assignmentNumber)


	let x = 200;
	let y = 18;

	console.log("x: "+ y);
	console.log("y: "+ x);

	// Addition
	let sum = x + y;
	console.log("Result of Add. operator: " + sum)

	// Subtarction

	let difference = x - y;
	console.log("Result of substraction operator: " + difference);

	// Multiplication
	let product = x * y;
	console.log("Result of multiplication operator" + product);

	// Division
	let quotient = x / y;
	console.log("Result of dividion operator" + quotient);

	let modulo = x % y
	console.log("Result of modulo operator" + modulo);

	// Continuation of assignment operator

	// long method Addition assignment operator ex: at top assignment = 8
		// assignmentNumber = assignmentNumber + 2;
		// console.log(assignmentNumber);

	// short method
	assignmentNumber += 2;
	console.log("Result of Addition assignment operator: " + assignmentNumber);
	// assignment value become 10 kasi na comment ang 8

	// assignmentNumber = assignmentNumber - 3;
	// console.log(assignmentNumber) value is 7

	assignmentNumber -= 3;
	console.log("Result of subtraction assignment operator: " + assignmentNumber);

	// multiplication long method
	// assignmentNumber = assignmentNumber * 2;
	// console.log(assignmentNumber)
	// short method
	assignmentNumber *= 2;
	console.log("Result of multiplication assignment operator: " + assignmentNumber);

	// division long method
	// assignmentNumber = assignmentNumber / 2;
	// console.log(assignmentNumber)
	// short method
	assignmentNumber /= 2;
	console.log("Result of dividion assignment operator: " + assignmentNumber);

// {SECTION} PEMDAS (ORDER OF OPERATION)
// multiple of operators and parenthesis

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operator: "+ mdas);
// The operations were done in the ff. order
// 1.) 3 * 4 = 12 / 1 + 2 - 12 / 5
// 2.) 12 / 5 = 2.4 / 1 + 2 - 2.4
// 3.) 1 + 2 = 3/3 -2.4
// 4.) 3 - 2.4 = 0.6

let pemdas = (1+(2-3)) * (4/5);
console.log("Result of pemdas operation: "+ pemdas)

// heirarchy 
// combinations of multiple arithmentic operators will follow the pemdas rule

/*/* 1. parenthesis
	  2. exponent
	  3. multiplication or division
	  4. addition or subtraction

	  note: will also follow left to right rule*/


	  // [section] increment and decrement
	  // increment-pagdagdag
	  // decrement-pagbawas

	  // operators that or subtract values by 1 and reasign the value of variable where the increment (++) / decrement (--) was applied

	  let z = 1;

	  // pre-increment = 2 = 2
	  let increment = ++z;
	  console.log("Result of pre-increment: " + increment);
	  console.log("Result of pre-increment of z: "+ z);

	  // post-increment =2 =3
	  increment = z++;
	  console.log("Result of post-increment: " + increment);
	  console.log("Result of pre-increment of z: " + z);

	 /* increment = z++;
	  console.log("Result of post-increment: " + increment);
	  console.log("Result of pre-increment of z: " + z);*/

	  // pre-decrement = 2 = 2
	  let decrement = --z;
	  console.log("Result of pre-decrement: "+ decrement);
	  console.log("Result of pre-decrement of z: " + z);


	  // post-decrement = 2 = 1
	  decrement = z--;
	  console.log("Result of post-decrement: "+ decrement);
	  console.log("Result of post-decrement of z: " + z);

	  // [section] type coercion
	  // is the automatic conversion of values from one data type to another

//  combination of number and string data type will result to string
	  let numA = 10; /*number*/
	  let numB = "12";  /*string*/

	  let coercion = numA + numB;
	  console.log(coercion);
	  console.log(typeof coercion);


// number will result to number
	  let numC = 16;
	  let numD = 14;
	  let nonCoercion = numC + numD;
	  console.log(nonCoercion);
	  console.log(typeof nonCoercion)


//  combination of number and boolean data type will result to number
	  let numX = 10;
	  let numY = true;
	  	//  boolean values are 
	  			//  true = 1;
	  			// false = 0;

	  coercion = numX + numY;
	  console.log(coercion);
	  console.log(typeof coercion);


	  let num1 = 1;
	  let num2 = false;
	  coercion = num1 + num2;
	  console.log(coercion);
	  console.log(typeof coercion);

	  // section comparison operators
	  // comparison opetrators are used to evaluate and compare the left and right operands )(==)
	  // after evaluation, it retuns a boolean value true and false

	  // equality operator 
	  /*compares the value, but not the data type*/
	  console.log(1 == 1); /*true*/
	  console.log(1 == 2) /*false*/
	  console.log(1 == '1'); /*true*/
	  console.log(0 == false); /*true*/ /*false boolean value is equal to zero*/


	  let juan = "juan";
	  console.log('juan' == 'juan'); /*true*/
	  console.log('juan' == 'Juan');/*FALSE*/ /*quality operator is strict with letter casing*/
	  console.log(juan == "juan"); /*true*/



	  // inequality operator (!=) also read us not equal
	  console.log(1 != 1);  /*false*/
	  console.log(1 != 2) /*true*/
	  console.log(1 != '1'); /*false*/
	  console.log(0 != false); /*false*/

	  console.log('juan' != 'juan');/*false*/
	  console.log('juan' != 'juan'); /*true*/
	  console.log(juan != "juan"); /*false*/

	  // [section] relational operators
	  // some comparison operators to check wether one value is greater than or less than
	  console.log(_____________________)
	  let a = 50;
	  let b = 65;

	  // GT or (>)
	  let isGreaterThan = a > b; /*50 > 65 = false*/
	  console.log(isGreaterThan);

	  // LT or (<)
	  let isLessThan = a > b; /*50 > 65 = true*/
	  console.log(isLessThan);

	  // GTE or greater than or eqaul (<=)
	  let isGTorEqual = a >= b; /*false*/
	  console.log(isGTorEqual)
	  // or any of will satisfy 



	  // LTE or LESS than or eqaul (<=)
	  let isLTorEqual = a >= b; /*false*/
	  console.log(isLTorEqual)
	  // or any of will satisfy 


	  // FORCE COERCION
	  let num = 56;
	  let numStr = "30";
	  console.log(num > numStr); /*TRUE*/
	  /*forced coercion to change string to number*/

	  let str = "twenty";
	  console.log(num >= str); /*false*/
	  // since the string is not numeric it will not read as number
	  // a.k.a (not a NUMBER)

	 

	 // logical (AND) operator (&&)
	 console.log("_____________")
	 let isLegalAge = true;
	 let isRegistered = false;

	 let allRequirementsMet = isLegalAge && isRegistered; /*false*/
	 console.log("Result of logical AND Operaor: "+ allRequirementsMet);
	 // AND operator requires all / both are true

 


 // logical (OR) operator (||)
 let someRequirementsMet = isLegalAge || isRegistered; /*true*/
 console.log("Result of logical OR Operaor: "+ someRequirementsMet);
 // OR operator requires only 1 true

 // LOGICAL NOT OPERATOR
 // RETURNS THE OPPOSITE VALUE
 let someRequirementsNotMet = isLegalAge; /*false*/
 console.log("Result of logical NOT Operaor: "+ someRequirementsNotMet);


